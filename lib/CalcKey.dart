import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CalcKey extends StatelessWidget {
  final String text;
  final GestureTapCallback onPressed;
  final GestureLongPressCallback onLongPress;

  const CalcKey({Key key, this.onPressed, this.text, this.onLongPress})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: SizedBox(
        height: 65,
        child: GestureDetector(
          onLongPress: onLongPress,
          child: RawMaterialButton(
            fillColor: Colors.deepPurple,
            splashColor: Colors.purpleAccent,
            shape: const CircleBorder(),
            child: Text(
              text,
              style: TextStyle(
                color: Colors.white,
                fontSize: 25,
              ),
            ),
            onPressed: onPressed,
          ),
        ),
      ),
    );
  }
}
