import 'package:calc_clone/CalcKey.dart';
import 'package:flutter/material.dart';

class CalcPage extends StatefulWidget {

  @override
  _CalcPageState createState() => _CalcPageState();
}

class _CalcPageState extends State<CalcPage> {
  String _billy = "";
  double _result = 0;

  void _concatBilly(String c) {
    setState(() {
      _billy += c;
    });
  }

  void _sup() {
    setState(() {
      var billy = new List<String>.from(_billy.split(''));
      billy.removeLast();
      _billy = billy.isEmpty ? "" : billy.reduce((curr, next) => curr + next);
    });
  }

  void _clear() {
    setState(() {
      _billy = "";
      _result = 0;
    });
  }

  _calc() {
    setState(() {
      if (_billy == "") {
        _result = 0;
      }else{
        var billist = new List<String>.from(_billy.split(''));
        String calcBuffer = '';
        for (int i = 0; i <= billist.length ; i++){
          int char = billist.elementAt(i).codeUnitAt(0);
          if (char <= 58 && char >= 47 ) {
            calcBuffer += billist.elementAt(i);
          } else {
            if (billist.elementAt(i) == '/'){

            }else if (billist.elementAt(i) == 'x'){

            }
          }
        }

        //_result = double.parse(_billy);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
        ),
        body: Container(
          color: Colors.black,
          child: DefaultTextStyle(
            style: TextStyle(color: Colors.white, fontSize: 45),
            child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          _billy,
                          style: TextStyle(color: Colors.grey),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        _result == 0 ? Text('') : Text(_result.toString()),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      CalcKey(
                        text: '=',
                        onPressed: () => _calc(),
                      ),
                    ],
                  ),
                  Table(children: [
                    TableRow(children: [
                      CalcKey(
                        text: '7',
                        onPressed: () => _concatBilly('7'),
                      ),
                      CalcKey(
                        text: '8',
                        onPressed: () => _concatBilly('8'),
                      ),
                      CalcKey(
                        text: '9',
                        onPressed: () => _concatBilly('9'),
                      ),
                      CalcKey(
                        text: '/',
                        onPressed: () => _concatBilly('/'),
                      ),
                    ]),
                    TableRow(children: [
                      CalcKey(
                        text: '4',
                        onPressed: () => _concatBilly('4'),
                      ),
                      CalcKey(
                        text: '5',
                        onPressed: () => _concatBilly('5'),
                      ),
                      CalcKey(
                        text: '6',
                        onPressed: () => _concatBilly('6'),
                      ),
                      CalcKey(
                        text: 'x',
                        onPressed: () => _concatBilly('x'),
                      ),
                    ]),
                    TableRow(children: [
                      CalcKey(
                        text: '1',
                        onPressed: () => _concatBilly('1'),
                      ),
                      CalcKey(
                        text: '2',
                        onPressed: () => _concatBilly('2'),
                      ),
                      CalcKey(
                        text: '3',
                        onPressed: () => _concatBilly('3'),
                      ),
                      CalcKey(
                        text: '-',
                        onPressed: () => _concatBilly('-'),
                      ),
                    ]),
                    TableRow(children: [
                      CalcKey(
                        text: ',',
                        onPressed: () => _concatBilly('.'),
                      ),
                      CalcKey(
                        text: '0',
                        onPressed: () => _concatBilly('0'),
                      ),
                      CalcKey(
                        text: 'c',
                        onPressed: () => _billy.isEmpty ? null : _sup(),
                        onLongPress: () => _billy.isEmpty ? null :_clear(),
                      ),
                      CalcKey(
                        text: '+',
                        onPressed: () => _concatBilly('+'),
                      ),
                    ]),
                  ])
                ]),
          ),
        ));
  }
}
